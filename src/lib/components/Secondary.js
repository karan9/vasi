import React, { Component } from 'react';
import recommend from './images/recommend.png';
import screening from './images/screening.png';
import interview from './images/interview.png';
import assistant from './images/messages.png';

class Secondary extends Component {
	state = {
		style: {
			display: 'block',
		},
	};


	componentDidMount() {
		if (this.props.login) {
			this.setState({
				style: {
					display: 'block',
				},
			});
		} else {
			this.setState({
				style: {
					display: 'none',
				},
			});
		}
	}

	handleBack = param => {
		console.log(param);
		this.props.status(param);
		this.props.subtaractTicker('general');
	};

	loadCat = param => {
		this.props.catstatus(param);
		if (param === 'screening') {
			this.props.subtaractTicker('sreening');
			this.props.startScreeening();
		}
		if (param === 'interview') {
			this.props.startInterview();
			this.props.subtaractTicker('interview');
		}
		if (param === 'Recommendation') {
			this.props.startRecommendation();
			this.props.subtaractTicker('Recommendation');
		}
	};

	render() {
		return (
			<div className="chatbot__collapsed">
				<div className="chatbot__sec-cat">
					<div className="chatbot__sect-item" onClick={this.handleBack}>
						<div class="chatbot__tooltip">
							<img
								id="general"
								style={{ width: '20px' }}
								src={assistant}
								alt="assistant not found"
								className="chatbot__sec-img"
							/>
							<span class="chatbot__tooltiptext">General chat</span>
						</div>

						{this.props.tic.tic_general ? (
							<div className="chatbot__number-general-container">
								<span className="chatbot__number">{this.props.tic.tic_general}</span>
							</div>
						) : (
							''
						)}
					</div>
					<div
						className="chatbot__sect-item"
						style={this.props.login ? {display:"block"}:{display:"none"}}
						onClick={e => this.loadCat('Recommendation', '', e)}
					>
						<div class="chatbot__tooltip">
							<img src={recommend} alt="recommend not found" className="chatbot__sec-img" />
							<span class="chatbot__tooltiptext">Recommendations</span>
						</div>

						{this.props.tic.tic_recommendation ? (
							<div className="chatbot__number-rec-container">
								<span className="chatbot__number">{this.props.tic.tic_recommendation}</span>
							</div>
						) : (
							''
						)}
					</div>
					<div
						style={this.props.login ? {display:"block"}:{display:"none"}}
						className="chatbot__sect-item"
						onClick={e => this.loadCat('screening', '', e)}
					>
						<div class="chatbot__tooltip">
							<img src={screening} alt="screening not found" className="chatbot__sec-img" />
							<span class="chatbot__tooltiptext">Screening</span>
						</div>

						{this.props.tic.tic_screening ? (
							<div className="chatbot__number-screen-container">
								<span className="chatbot__number">{this.props.tic.tic_screening}</span>
							</div>
						) : (
							''
						)}
					</div>
					<div
						className="chatbot__sect-item"
						style={this.props.login ? {display:"block"}:{display:"none"}}
						onClick={e => this.loadCat('interview', '', e)}
					>
						<div class="chatbot__tooltip">
							<img src={interview} alt="interview not found" className="chatbot__sec-img" />
							<span className="chatbot__tooltiptext">Interview</span>
						</div>

						{this.props.tic.tic_interview ? (
							<div className="chatbot__number-interview-container">
								<span className="chatbot__number">{this.props.tic.tic_interview}</span>
							</div>
						) : (
							''
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default Secondary;
