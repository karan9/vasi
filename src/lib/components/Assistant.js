import React, { Component } from 'react';
import assistant from './images/assistant2.png';


class Assistant extends Component{

  render(){


    return(
      <div className="chatbot__bot-assistant"><img src={assistant} alt="assistant pic not available" /></div>
    )
  }
}

export default Assistant;
