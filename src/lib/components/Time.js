var dateFormat = require('dateformat');
var moment = require('moment');


var now = new Date();
export default function getCurrentTime(dateobj) {
    dateFormat.masks.mytime = 'hh:MM TT';
    var  time = dateFormat(now, "mytime");
    return time;

    
  }

  export  function getRelativeTime(prevdate) {
    if(prevdate){
      dateFormat.masks.mytime = 'hh:MM TT';
  var  daysdiff = moment(moment().format()).diff(  prevdate, 'days');
  if(daysdiff === 0 ){
    var time  = "Today "+ dateFormat(prevdate, "mytime");
  }
  else if(daysdiff === 1){
    var time  = "Yesterday "+ dateFormat(prevdate, "mytime");

  }
  else{
    var time =  dateFormat(prevdate,"mmmm dS, yyyy hh:MM");
  }
  // var  time = moment().subtract(daysdiff, 'days').calendar()
    }
    
    
  
    
    return time;

    
  }