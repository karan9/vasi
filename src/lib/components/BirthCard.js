import React, { Component } from 'react';
import assistant from './images/assistant2.png';




class BirthCard extends Component{

  render(){

    return(
  <div className="chatbot__birth-container">
    <div className="chatbot__bot-assistant"><img src={assistant} alt="assistant pic not available" /></div>
  <div className="chatbot__birth-card">

  <div className="chatbot__birth-card-icon"><img src={assistant} alt="birth-card-icon not available" /></div>
  <div className="chatbot__birth-wish">
  <div className="chatbot__wish-heading">{this.props.msg.heading}</div>
  <div className="chatbot__wish-text">{this.props.msg.text}</div>

  </div>


  </div>
</div>





    )
  }
}

export default BirthCard;
