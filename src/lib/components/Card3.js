import React, { Component } from 'react';
import assistant from './images/assistant1.png';




class Card3 extends Component{

  render(){

    return(
  <div className="chatbot__assistant-card">


       <img className="chatbot__assistant-icon" src={assistant} alt="assistant-icon not available" />
        <div className="chatbot__assistant-details">
        <span className="chatbot__assistant-name">Vino</span>
        <span className="chatbot__assistant-tag">|Your personal job assistant</span>
        <span className="chatbot__assistant-text"> I can always recommend ...</span>
        <div className="chatbot__day-time">Today 5:00 pm</div>
      </div>

  </div>

    )
  }
}

export default Card3;
