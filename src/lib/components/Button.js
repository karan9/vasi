import React, { Component } from 'react';

class Button extends Component {
	state = {
		active: 'chatbot__first-button-active',
		inactive: 'chatbot__first-button',
		count: 0,
		copy: this.props.buttondata,
	};
	componentDidMount(){
		//this.props.disableInput();
	}
	changeButtonState = data => {
		if(!this.props.active){
		
		this.props.addParams(data,this.props.id);
		let keys = Object.keys(this.refs);

		keys.map(key => {
			this.refs[key.toString()].className = this.state.inactive;
		});
		this.refs[data].className = this.state.active;
	}
	else{
		// let keys = Object.keys(this.refs);

		// keys.map(key => {
		// 	this.refs[key.toString()].className = this.state.inactive;
		// });
		// this.refs[data].className = this.state.active;
		
	}
	};

	render() {
		const buttonList = this.state.copy.map(button => {
			if(this.props.active === button){
				return(<div className={this.state.active} ref={button} onClick={ e => this.changeButtonState(button)}>
				{button}
			</div>)
			}
			else{
			return (
				<div className={this.state.inactive} ref={button} onClick={e => this.changeButtonState(button)}>
					{button}
				</div>
			);
		}
		});
		console.log(buttonList);
		return <div className="chatbot__buttons">{buttonList}</div>;
	}
}

export default Button;
