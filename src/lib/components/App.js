import React, { Component } from 'react';
import './App.css';
import Title from './Title.js';
import Secondary from './Secondary.js';
import Catagory from './Catagory.js';
import Chat from './Chat.js';
import axios from 'axios';

import io from 'socket.io-client';

const socket = io('http://149.129.137.48:5000', { transports: ['websocket'] });

const VISIBILITY_STYLE_MAP = {
	EXPAND: '320px',
	MINIMIZE: window.screen.availWidth <= 1024 ? '0px' : '43px',
	MAXIMIZE: window.screen.availWidth <= 1024 ? '100%' : '320px',
};

class App extends Component {
	state = {
		loader: 'none',
		systemId: null,
		reload: true,
		eofmessage: 'chatbot__message_end',
		input: 'chatbot__message-box',
		back_style: 'block',
		tic: {
			tic_screening: 0,
			tic_recommendation: 0,
			tic_interview: 0,
			tic_general: 0,
		},
		search_key: '@nbsdh930',
		jobIds: [],
		temp: [],
		vis: { display: '' },
		grid: false,
		login: false,
		style: {
			width: '320px',
		},
		search_counter: 0,
		current_counter: 1,
		user_data: {
			userId: '',
			appId: '',
			appliStatus: 'pending',
			jobId: '',
		},
		mobile: false,
		general: [
			{
				type: 'bot',
				content: ['Hii,  I am Vasi', 'I can help you with following things'],
			},
			{ type: 'button', content: ['Jobs', 'Candidates'], id: 'abcd' },
		],
		screeningLoad: true,
		process: 'running',
		screeningStatus: 'start',
		interviewLoad: true,
		interviewStatus: 'start',
		screening_status_card: [],
		interview_status_card: [],
		recommendation_status_card: [],
		buffer: [],
		screening: [],
		interview: [
			{
				type: 'bot',
				content: ['Hii, I am Vino your Interview Scheduling assistant'],
			},
		],
		messages: [],
		mode: 'general',
		catagory: false,
		chat: false,
		cat_card: {
			RecommendationCard: true,
			ScreeningCard: true,
			InterviewCard: true,
			ChatCard: true,
		},
		cat_style: {
			display: 'none',
		},
		time: {
			rec_time: 'No Recommendations yet',
			screen_time: 'No Screeningin yet',
			interview_time: 'No Interview yet',
		},
		dataload: false,
	};

	changeJobId = (param, process) => {
		console.log(param, this.state.user_data.jobId);

		if (process === 'screening') {
			if (param !== this.state.user_data.jobId) {
				this.state.jobIds.map(job => {
					if (job.jobId === param) {
						console.log(job.jobId, job.screening);
						this.setState({
							screening: job.screening,
							screeningLoad: job.screeningLoad,
							screeningStatus: job.screeningStatus,
							interviewLoad: job.interviewLoad,
							interviewStatus: job.interviewStatus,
							reload: job.reload,
							messages: job.screening,
						});
						if (job.screeningLoad) {
							this.enableInput();
						}
					}
				});
			}
		}
		if (process === 'interview') {
			console.log(param, this.state.user_data.jobId);
			if (param !== this.state.user_data.jobId) {
				this.state.jobIds.map(job => {
					if (job.jobId === param) {
						this.setState({
							interview: job.interview,
							screeningLoad: job.screeningLoad,
							screeningStatus: job.screeningStatus,
							interviewLoad: job.interviewLoad,
							interviewStatus: job.interviewStatus,
							reload: job.reload,
							messages: job.interview,
						});
						if (job.interviewLoad) {
							this.enableInput();
						}
					}
				});
			}
		}
		let { user_data } = this.state;
		this.setState({
			user_data: {
				...user_data,
				jobId: param,
			},
		});
	};

	getRelativeTime = prevdate => {
		var dateFormat = require('dateformat');
		var moment = require('moment');
		if (prevdate) {
			dateFormat.masks.mytime = 'hh:MM TT';
			var daysdiff = moment(moment().format()).diff(prevdate, 'days');
			if (daysdiff === 0) {
				var time = 'Today ' + dateFormat(prevdate, 'mytime');
			} else if (daysdiff === 1) {
				var time = 'Yesterday ' + dateFormat(prevdate, 'mytime');
			} else {
				var time = dateFormat(prevdate, 'mmmm dS, yyyy hh:MM');
			}
			// var  time = moment().subtract(daysdiff, 'days').calendar()
		}

		return time;
	};
	resetkey = () => {
		this.setState({
			search_key: '$owjd0',
		});
	};
	addTicker = param => {
		if (param === 'screening') {
			this.setState({
				tic: {
					...this.state.tic,
					tic_screening: this.state.tic.tic_screening + 1,
				},
			});
		} else if (param === 'interview') {
			this.setState({
				tic: {
					...this.state.tic,
					tic_interview: this.state.tic.tic_interview + 1,
				},
			});
		} else if (param === 'general') {
			this.setState({
				tic: {
					...this.state.tic,
					tic_general: this.state.tic.tic_general + 1,
				},
			});
		} else {
			this.setState({
				tic: {
					...this.state.tic,
					tic_recommendation: this.state.tic.tic_recommendation + 1,
				},
			});
		}
	};

	subtaractTicker = param => {
		if (param === 'screening') {
			if (this.state.tic.tic_screening !== 0) {
				this.setState({
					tic: {
						...this.state.tic,
						tic_screening: 0,
					},
				});
			}
		} else if (param === 'interview') {
			if (this.state.tic.tic_interview !== 0) {
				this.setState({
					tic: {
						...this.state.tic,
						tic_interview: 0,
					},
				});
			}
		} else if (param === 'general') {
			if (this.state.tic.tic_general !== 0) {
				this.setState({
					tic: {
						...this.state.tic,
						tic_general: 0,
					},
				});
			}
		} else {
			if (this.state.tic.tic_recommendation !== 0) {
				this.setState({
					tic: {
						...this.state.tic,
						tic_recommendation: 0,
					},
				});
			}
		}
		this.saveState();
	};

	setSearch = param => {
		this.setState({
			search: false,
		});
	};

	setButton = param => {
		this.setState({
			button: false,
		});
	};
	setEof = () => {
		this.setState({
			eofmessage: 'chatbot__message_end',
		});
	};
	resetEof = () => {
		this.setState({
			eofmessage: '',
		});
	};

	addDummy = () => {
		// let message = { id: '1ml0kdls7', type: 'test', content: ['test'] };
		// let messages = [...this.state.messages, message];
		// this.setState({
		// 	messages: messages,
		// });
	};
	removeDummy = () => {
		let left = this.state.messages;

		for (var i = left.length; i--; ) {
			if (left[i].type === 'test') left.splice(i, 1);
		}

		this.setState({
			messages: left,
		});
	};

	incCounter = () => {
		if (this.state.current_counter < this.state.search_counter) {
			this.setState({
				current_counter: this.state.current_counter + 1,
			});
		} else {
			this.setState({
				current_counter: 1,
			});
		}
	};
	decCounter = () => {
		if (this.state.current_counter > 2) {
			this.setState({
				current_counter: this.state.current_counter - 1,
			});
		} else {
			this.setState({
				current_counter: this.state.search_counter,
			});
		}
	};

	searchResult = param => {
		this.setState({
			search_key: param,
		});

		console.log('search is happening');

		this.removeDummy();

		var count = 0;
		var messages = this.state.messages;
		for (var i = 0; i < messages.length; i++) {
			for (var j = 0; j < messages[i].content.length; j++) {
				//console.log( messages[i].content[j].includes(param));
				if (messages[i].content[j].includes(param)) {
					count++;
				}
			}
		}
		this.setState({
			search_counter: count,
		});
		let newcount = 0;
		for (var i = 0; i < messages.length; i++) {
			for (var j = 0; j < messages[i].content.length; j++) {
				//console.log( messages[i].content[j].includes(param));
				if (messages[i].content[j].includes(param)) {
					newcount++;
					if (this.state.current_counter === newcount) {
						let left = messages.slice(0, i);
						let right = messages.slice(i, this.state.messages.length);
						left.push({ id: 'hod2n39sld8ays6s9', content: ['test'], type: 'test' });
						left.push.apply(left, right);
						this.setState({
							messages: left,
						});
						console.log(left);
						break;
					}
				}
			}
		}

		console.log('searches found ', count);
	};

	enableInput = () => {
		this.setState({
			input: 'chatbot__message-box',
		});
		this.saveState();
	};
	disableInput = () => {
		this.setState({
			input: 'chatbot__message-box-alt',
		});
		this.saveState();
	};

	getState = (screening = false) => {
		console.log('Get state called with screening', screening, this.state.user_data.userId);
		axios
			.get(
				'http://149.129.137.48:5009/api/GetState/' +
					this.state.user_data.userId +
					':' +
					this.state.user_data.jobId
			)
			.then(res => {
				let tempstate = res.data;

				console.log('got the data ');
				if ('data' in tempstate) {
					console.log('user has no data yet');
					if (screening === true) {
						this.initialProcess();
					}
				} else {
					console.log('user had done some activity');
					let found = false;
					console.log('data fetched --', tempstate);
					tempstate.jobIds.map(job => {
						if (job.jobId === this.state.user_data.jobId) {
							console.log('Job found in database');
							found = true;
							(tempstate.screening = job.screening),
								(tempstate.interview = job.interview),
								(tempstate.screeningLoad = job.screeningLoad),
								(tempstate.interviewLoad = job.interviewLoad),
								(tempstate.reload = job.reload),
								(tempstate.screening_status_card = job.screening_status_card),
								(tempstate.interview_status_card = job.interview_status_card),
								(tempstate.screeningStatus = job.screeningStatus),
								(tempstate.interviewStatus = job.interviewStatus);
						}
					});

					// this.setState({
					// 	...this.state,
					// 	tempstate
					// 	// screening: tempstate.screening,
					// 	// interview: tempstate.interview,
					// 	// screeningLoad: tempstate.screeningLoad,
					// 	// interviewLoad: tempstate.interviewLoad,
					// 	// reload: tempstate.reload,
					// 	// screening_status_card: tempstate.screening_status_card,
					// 	// interview_status_card: tempstate.interview_status_card,

					// });
					if (found === true) {
						console.log('data recovered');
						this.setState(tempstate);
					} else {
						console.log('job id changed but still some data need to be refreshed');

						this.setState({
							general: tempstate.general,
							recommendation_status_card: tempstate.recommendation_status_card,
							screening_status_card: tempstate.screening_status_card,
							interview_status_card: tempstate.interview_status_card,
							jobIds: tempstate.jobIds,
							screeningLoad: true,
							interviewLoad: true,
							screening: [],
							interview: [],
							screeningStatus: 'start',
							interviewStatus: 'start',
							reload: true,
						});

						this.initialProcess();
					}
					//console.log(this.state, tempstate.screeningLoad);
				}
			}).catch(err=>{
        console.log("GET API DID NOT CALLED",err);
      });;
	};

	RecoverState = () => {
		return new Promise((resolve, reject) => {
			console.log('Recover state called');
			axios
				.get(
					'http://149.129.137.48:5009/api/GetState/' +
						this.state.user_data.userId +
						':' +
						this.state.user_data.jobId
				)
				.then(res => {
					let tempstate = res.data;

					//console.log('got the data ');
					if ('data' in tempstate) {
						console.log('user has no data yet');
					} else {
						this.setState(tempstate);
						console.log('Recover state called data recovered');

						// console.log('user had done some activity');
						// let found = false;
						// console.log('data fetched --', tempstate);
						// tempstate.jobIds.map(job => {
						// 	if (job.jobId === this.state.user_data.jobId) {
						// 		console.log('Job found in database');
						// 		found = true;
						// 		(tempstate.screening = job.screening),
						// 			(tempstate.interview = job.interview),
						// 			(tempstate.screeningLoad = job.screeningLoad),
						// 			(tempstate.interviewLoad = job.interviewLoad),
						// 			(tempstate.reload = job.reload),
						// 			(tempstate.screening_status_card = job.screening_status_card),
						// 			(tempstate.interview_status_card = job.interview_status_card),
						// 			(tempstate.screeningStatus = job.screeningStatus),
						// 			(tempstate.interviewStatus = job.interviewStatus);
						// 	}
						// });

						// // this.setState({
						// // 	...this.state,
						// // 	tempstate
						// // 	// screening: tempstate.screening,
						// // 	// interview: tempstate.interview,
						// // 	// screeningLoad: tempstate.screeningLoad,
						// // 	// interviewLoad: tempstate.interviewLoad,
						// // 	// reload: tempstate.reload,
						// // 	// screening_status_card: tempstate.screening_status_card,
						// // 	// interview_status_card: tempstate.interview_status_card,

						// // });
						// if (found === true) {
						// 	console.log('data recovered');
						// 	this.setState(tempstate);
						// 	//const { user } = this.props;
						// 	// if (user.id) {
						// 	// 	this.setState({
						// 	// 		login: true,
						// 	// 		user_data: {
						// 	// 			...this.state.user_data,
						// 	// 			userId: user.id,
						// 	// 		},
						// 	// 	});
						// 	// 	console.log(this.state.login);
						// 	// } else {
						// 	// 	this.setState({
						// 	// 		login: false,
						// 	// 	});
						// 	// }
						// } else {
						// 	console.log('job id changed but still some data need to be refreshed');

						// 	this.setState({
						// 		general: tempstate.general,
						// 		recommendation_status_card: tempstate.recommendation_status_card,
						// 		screening_status_card: tempstate.screening_status_card,
						// 		interview_status_card: tempstate.interview_status_card,
						// 		jobIds: tempstate.jobIds,
						// 		screeningLoad: true,
						// 		interviewLoad: true,
						// 		screening: [],
						// 		interview: [],
						// 		screeningStatus: 'start',
						// 		interviewStatus: 'start',
						// 		reload: true,
						// 	});

						// 	this.initialProcess();
						// }
						// //console.log(this.state, tempstate.screeningLoad);
					}

					resolve();
				})
				.catch(err => {
					reject();
				});
		})
	};

	saveState = () => {
		let found = false;
		let jobs = this.state;
		jobs.jobIds.map(jobdata => {
			//console.log(jobdata);

			if (jobdata.jobId === this.state.user_data.jobId) {
				//console.log("matched");
				(found = true),
					(jobdata['screeningStatus'] = this.state.screeningStatus),
					(jobdata['interviewStatus'] = this.state.interviewStatus),
					(jobdata['screening'] = this.state.screening),
					(jobdata['screeningLoad'] = this.state.screeningLoad),
					(jobdata['screening_status_card'] = this.state.screening_status_card),
					(jobdata['interview'] = this.state.interview),
					(jobdata['interviewLoad'] = this.state.interviewLoad),
					(jobdata['interview_status_card'] = this.state.interview_status_card),
					(jobdata['process'] = this.state.process),
					(jobdata['reload'] = this.state.reload);
			}
		});

		if (found === true) {
			this.setState({
				jobIds: jobs.jobIds,
			});
		} else {
			console.log(' not matched');
			let job = {
				jobId: this.state.user_data.jobId,
				screeningStatus: this.state.screeningStatus,
				interviewStatus: this.state.interviewStatus,
				screening: this.state.screening,
				screeningLoad: this.state.screeningLoad,
				screening_status_card: this.state.screening_status_card,
				interview: this.state.interview,
				interviewLoad: this.state.interviewLoad,
				interview_status_card: this.state.interview_status_card,
				process: this.state.process,
				reload: this.state.reload,
			};
			jobs.jobIds.push(job);
			console.log(job);
			this.setState({
				jobIds: jobs.jobIds,
			});
		}

		axios
			.post(
				'http://149.129.137.48:5009/api/SaveState',
				{
					data: this.state,
				},
				{
					headers: {
						'Content-Type': 'application/json',
						//            'Content-Type': 'multipart/form-data'
						//	'Content-Type': 'application/x-www-form-urlencoded'
					},
				}
			)
			.then(res => {
				console.log(res.data.data);
				console.log('data saved sucessfully');
			});
	};

	changeToScreenCard = param => {
		this.subtaractTicker('general');
		this.setState({
			chat: false,
			catagory: true,
			mode: 'screening',
			grid: false,
			cat_card: {
				RecommendationCard: false,
				ScreeningCard: false,
				InterviewCard: false,
			},
		});
		this.saveState();
	};
	changeToInterviewCard = param => {
		this.subtaractTicker('interview');
		this.setState({
			chat: false,
			catagory: true,
			mode: 'interview',
			grid: false,
			cat_card: {
				RecommendationCard: false,
				ScreeningCard: false,
				InterviewCard: false,
			},
		});
		this.saveState();
	};
	changeToRecommendCard = param => {
		this.subtaractTicker();
		console.log('nzxjcbhnlmzx');
		this.setState({
			chat: false,
			catagory: true,
			mode: 'Recommendation',
			grid: false,
			cat_card: {
				RecommendationCard: false,
				ScreeningCard: false,
				InterviewCard: false,
			},
		});
		this.saveState();
	};
	changeMode = mode => {
		this.setState({
			mode: mode,
		});
	};

	changeGrid = () => {
		this.setState({
			grid: !this.state.grid,
		});
		this.setState({
			mode: 'catagory',
		});
	};
	reset = () => {
		this.setState({
			general: [],
		});
	};
	keyGenrator = () => {
		const uuidv4 = require('uuid/v4');
		return uuidv4();
	};

	changeCatStatus = (param, param2) => {
		if (param === 'Recommendation') {
			if (this.props.actions) {
				this.props.actions.onExpand();
				this.setState({
					vis: { display: '' },
				});
			}
			this.setState({
				cat_style: {
					display: 'none',
				},
				cat_card: {
					RecommendationCard: true,
					ScreeningCard: false,
					InterviewCard: false,
				},
				mode: 'Recommendation',
				catagory: true,
			});
		} else if (param === 'screening') {
			if (this.props.actions) {
				this.props.actions.onExpand();
				this.setState({
					vis: { display: '' },
				});
			}
			this.setState({
				cat_style: {
					display: 'none',
				},
				cat_card: {
					RecommendationCard: false,
					ScreeningCard: true,
					InterviewCard: false,
				},
				mode: 'screening',
				messages: this.state.screening,
				catagory: true,
				grid: false,
			});
		} else if (param === 'interview') {
			if (this.props.actions) {
				this.props.actions.onExpand();
				this.setState({
					vis: { display: '' },
				});
			}
			this.setState({
				cat_style: {
					display: 'none',
				},
				cat_card: {
					RecommendationCard: false,
					ScreeningCard: false,
					InterviewCard: true,
				},
				mode: 'interview',
				messages: this.state.interview,
				catagory: true,
				grid: false,
			});
		} else {
			this.setState({
				cat_style: {
					display: '',
				},
				cat_card: {
					RecommendationCard: true,
					ScreeningCard: true,
					InterviewCard: true,
					ChatCard: true,
				},
				catagory: true,
				messages: this.state.general,
				chat: false,
			});
		}
	};
	changeChatStatus = param => {
		if (param === 'cat') {
			if (this.props.actions) {
				this.props.actions.onMinimize();
			}
			if (this.state.mode === 'general') {
				if (this.state.grid) {
					this.setState({
						chat: true,
						catagory: false,
						grid: false,
					});
				} else {
					if (this.props.actions) {
						this.props.actions.onExpand();
						this.setState({
							vis: { display: '' },
						});
					}
					this.setState({
						chat: false,
						catagory: true,
						grid: true,
						cat_style: {
							display: '',
						},
					});
				}
			} else if (this.state.mode === 'screening') {
				this.setState({
					catagory: false,
					chat: false,
				});
				if (this.props.actions) {
					this.props.actions.onMinimize();
				}
			} else if (this.state.mode === 'interview') {
				this.setState({
					catagory: false,
					chat: false,
				});
				if (this.props.actions) {
					this.props.actions.onMinimize();
				}
			} else {
				this.setState({
					catagory: false,
				});
			}
		} else if (param === 'close') {
			if (this.state.mobile) {
				if (this.props.actions) {
					this.props.actions.onMinimize();
				}
				this.setState({
					vis: { display: 'none' },
				});
			} else {
				this.setState({
					catagory: false,
					chat: false,
				});
				if (this.props.actions) {
					this.props.actions.onMinimize();
				}
			}
		} else if (param === 'expand') {
			if (this.state.style.width === '100%') {
				/*this.setState({
          style: {
            width: "320px"
          }
        }); */
				//console.log('needed to expand here');
				if (this.props.actions) {
					this.props.actions.onExpand();
					this.setState({
						vis: { display: '' },
					});
				}
			} else {
				// this.setState({
				//   style: {
				//     width: "100%"
				//   }
				// });
				//console.log('needed to maximize here');

				if (this.props.actions) {
					this.props.actions.onMaximize();
					this.setState({
						vis: { display: '' },
					});
				}
			}
		} else {
			if (this.props.actions) {
				this.props.actions.onExpand();
				this.setState({
					vis: { display: '' },
				});
			}

			this.setState({
				chat: !this.state.chat,
				mode: 'general',
				back_style: 'none',
			});

			this.showGeneral();
		}
	};

	checkAndUpdateVisibility = (visibility, currentStyle) => {
		//	console.log('---------------------------', visibility);

		if (currentStyle === VISIBILITY_STYLE_MAP[visibility]) {
			//	console.log('currentStyle  is returned', VISIBILITY_STYLE_MAP[visibility], currentStyle);
			return;
		}
		//console.log('checkAndUpdateVisibility is called');

		this.setState(
			{
				style: {
					width: VISIBILITY_STYLE_MAP[visibility],
				},
			},
			function() {
				console.log('state changed');
			}
		);
	};

	initialProcess = () => {
		if (this.state.screeningLoad === true || this.state.screeningStatus === 'start') {
			this.setState({
				mode: 'screening',
			});

			axios
				.get(
					'http://149.129.137.48:5009/api/data/' +
						this.state.user_data.userId +
						':' +
						this.state.user_data.jobId +
						':' +
						this.state.user_data.appId
				)
				.then(res => {
					let cards = res.data;
					let time = '';
					cards.map(data => {
						time = data.time;
					});

					this.setState({
						screening_status_card: [...this.state.screening_status_card, cards[0]],
					});
					var dateFormat = require('dateformat');
					var now = new Date();
					this.setState({
						time: {
							...this.state.time,
							screen_time: this.getRelativeTime(dateFormat(now, 'isoDateTime')),
						},
					});
					this.saveState();

					console.log('first api called', cards);

					axios
						.get(
							'http://149.129.137.48:5009/api/send_initial_screening_message/' +
								this.state.user_data.jobId +
								'/' +
								this.state.user_data.userId +
								':' +
								'first'
						)
						.then(res => {
							let messages = res.data;

							this.addApiMessages(messages);
							console.log('second api calls', messages);
							this.setState({
								screeningStatus: 'running',
							});
							this.saveState();

							console.log(this.state.screeningLoad, this.state.reload);
							if (this.state.reload === true) {
								if (this.state.screeningLoad === true) {
									console.log('third api calls');
									axios
										.get(
											'http://149.129.137.48:5009/api/send_message/' +
												this.state.user_data.userId +
												'/' +
												this.state.user_data.jobId +
												'/yes'
										)
										.then(res => {
											let messages = res.data;
											this.addApiMessages(messages);
											this.setState({
												reload: false,
											});
										});

									this.setState({
										reload: false,
									});
									this.saveState();
								}
							}
							this.setState({
								chat: true,
								mode: 'screening',
							});

							this.setState({
								screeningLoad: true,
								interviewLoad: true,
								grid: false,
							});
						});
				});
		}
	};

	checkAndUpdateApplication = application => {
		if (!application) {
			console.log('application not found');
			return;
		}
		const { jobId, applicationId, userId } = application;
		const { user_data } = this.state;

		//	console.log('Application ad user data ----', user_data, jobId, applicationId, userId);
		if (!jobId || !applicationId || !userId) {
			return;
		}

		if (user_data.appId === applicationId) {
			return;
		}

		this.setState(
			{
				user_data: {
					...user_data,
					userId: userId,
					appId: applicationId,
					jobId: jobId,
				},
			},
			() => {
				if (this.props.actions) {
					this.props.actions.onExpand();
					this.setState({
						vis: { display: '' },
					});
				}

				this.getState(true);

				console.log('-----------------------------get state finished---------------');
			}
		);
	};

	componentWillUnmount() {
		console.log('catagory unmounted');
		this.setState({
			process: 'aborted',
		});
	}
	componentDidUpdate() {
		console.log('component update called');
		const { cb } = this.props;
		const { style } = this.state;
		const { user } = this.props;

		//		console.log('Component Did Update Called', cb, style);

		if (!cb) {
			//		console.log('cb object is returned ');
			return;
		}
		if (user.id && !this.state.user_data.userId) {
			this.setState({
				login: true,
				user_data: {
					...this.state.user_data,
					userId: user.id,
				},
			});
			this.getState(false);
		}
		if (!user.id && this.state.login) {
			this.setState({
				login: false,
			});
		}

		this.checkAndUpdateVisibility(cb.visibility, style.width);
		this.checkAndUpdateApplication(cb.applicationData);
		if (window.screen.availWidth <= 1024) {
			if (cb.visibility === 'MINIMIZE') {
				if (this.state.vis.display === 'none') {
					return;
				}

				this.setState({
					vis: {
						display: 'none',
					},
				});
				return;
			}
			if (cb.visibility === 'MAXIMIZE') {
				if (this.state.vis.display === '') {
					return;
				}
				this.setState({
					vis: {
						display: '',
					},
				});
				return;
			}
		}
		if (cb.visibility === 'EXPAND') {
			if (this.state.vis.display === '') {
				return;
			}
			this.setState({
				vis: {
					display: '',
				},
			});
			return;
		}
	}

	componentDidMount() {
		const { cb, user } = this.props;

		// if (cb) {
		// 	this.setState({
		// 		systemId: cb.systemId,
		// 	});
		// }

		if (user.id) {
			this.setState(
				{
					login: true,
					user_data: {
						...this.state.user_data,
						userId: user.id,
					},
				},
				() => {
          this.getState(false);
          console.log("Component Did Mount Get State Called");
				}
			);
		} else {
			this.setState({
				login: false,
			});
		}

		console.log('component mount called');

		socket.on('connect', function(data) {
			// console.log(data);
		});

		socket.on(
			'Recommendation',
			function(message) {
				var dateFormat = require('dateformat');
				var now = new Date();
				console.log(dateFormat(now, 'isoDateTime'));

				this.setState({
					time: {
						...this.state.time,
						rec_time: this.getRelativeTime(dateFormat(now, 'isoDateTime')),
					},
				});
				message = JSON.parse(message);
				let userId = message.userId;
				message = message.data;
				if (this.state.user_data.userId === userId) {
					let data = [];
					if (Array.isArray(message)) {
						data = this.state.recommendation_status_card;
						message.map(one => {
							data.push(one);
							this.addTicker('Recommendation');
						});
						this.setState({
							recommendation_status_card: data,
						});
					} else {
						message = JSON.parse(message);
						data = [...this.state.recommendation_status_card, message];
						this.addTicker('Recommendation');
					}

					//	console.log(data);

					this.setState({
						recommendation_status_card: data,
					});

					this.saveState();
				}
				//	console.log(message);
			}.bind(this)
		);

		socket.on(
			'Engagement',
			function(message) {
				message = JSON.parse(message);
				console.log(message);
				let userId = message.userId;
				message = message.data;

				//console.log(Array.isArray(message),message)
				if (this.state.user_data.userId === userId) {
					if (Array.isArray(message)) {
						let data = this.state.general;
						message.map(one => {
							data.push(one);
						});
						if (this.state.mode === 'general' && this.state.grid === false) {
							this.setState({
								general: data,
								messages: data,
							});
						} else {
							this.setState({
								general: data,
							});
						}
					} else {
						let data = [...this.state.general, message];
						if (this.state.mode === 'general' && this.state.grid === false) {
							this.setState({
								general: data,
								messages: data,
							});
						} else {
							this.setState({
								general: data,
							});
						}
					}

					this.saveState();

					this.addTicker('general');
				}
			}.bind(this)
		);
		this.saveState();
		console.log('window width ------------------------' + window.screen.availWidth);
		if (window.screen.availWidth <= 1024) {
			this.setState({
				mobile: true,

				chat: false,
				catagory: true,
				grid: true,
				cat_style: {
					display: '',
				},
			});
			if (this.props.actions) {
				this.props.actions.onMinimize();
			}

			this.setState({
				vis: {
					display: 'none',
				},
			});
		}

		if (cb) {
			this.setState({
				style: {
					width: VISIBILITY_STYLE_MAP[cb.visibility],
				},
			});
		}
	}
	startScreeening = jobId => {};
	startInterview = jobId => {};

	startRecommendation = () => {};

	addApiMessages = message => {
		this.setState({
			loader: 'none',
		});

		message.id = this.keyGenrator();
		console.log('adding api messages', message.type);
		if (
			message.type === 'button' ||
			message.type === 'option' ||
			message.type === 'Card1' ||
			message.type === 'Card2'
		) {
			this.disableInput();
			this.addDummy();
		} else {
			this.enableInput();
		}
		if (this.state.mode === 'screening') {
			if (this.state.screeningLoad === false && this.state.interviewLoad === true) {
				let interview_messages = [...this.state.interview, message];
				let messages = [...this.state.messages, message];

				this.setState({
					interview: interview_messages,

					messages: messages,
				});
				this.addDummy();
			} else {
				let messages = [...this.state.screening, message];
				this.setState({
					screening: messages,
					messages: messages,
				});
				this.addDummy();
			}
		} else if (this.state.mode === 'interview') {
			let messages = [...this.state.interview, message];
			this.setState({
				interview: messages,
				messages: messages,
			});
			this.addDummy();
			this.saveState();
		} else if (this.state.mode === 'general') {
			if (Array.isArray(message)) {
				let messages = this.state.messages;
				message.map(one => {
					messages.push(one);
				});
				this.setState({
					general: messages,
					messages: messages,
				});
				this.addDummy();
				this.saveState();
			} else {
				if (message.mode === 'screening') {
					let messages = [...this.state.screening, message];
					this.setState({
						screening: messages,
						messages: messages,
					});
					this.addDummy();
					this.saveState();
				} else if (message.mode === 'interview') {
					let messages = [...this.state.interview, message];
					this.setState({
						interview: messages,
						messages: messages,
					});
					this.addDummy();
					this.saveState();
				} else {
					let messages = [...this.state.general, message];
					this.setState({
						general: messages,
						messages: messages,
					});
					this.addDummy();
					this.saveState();
				}
			}
		}
	};
	addParams = (param, param2) => {
		let messages = this.state.messages;
		messages.map(message => {
			console.log(message.id === param2);
			if (message.id === param2) {
				message.active = param;
			}
		});
		this.setState({
			messages: messages,
		});
		this.saveState();
		console.log('---add param', messages, param2);
		let message = {};
		message.content = [param];
		this.addMessages(message);
		if (this.state.mode === 'screening') {
			/* axios.get('http://192.168.0.4:5006/api/send_message/5b58c436ec17c509a498f43b/'+param)
        .then(res => {

          let message = res.data;
          this.addApiMessages(message);
        })*/
		}
	};
	addProcessing = () => {
		let process = { id: 'nsbdbnsi2', type: 'processing', content: [] };
		this.setState({
			messages: [...this.state.messages, process],
		});
	};
	removeProcessing = () => {
		// let messages = this.state.messages;
		// messages
	};
	addMessages = (message, dir) => {
		this.setState({
			loader: 'block',
		});
		console.log('adding messages');
		// if (message.type === 'button' || message.type === 'option' ||  message.type === 'Card1' || message.type === 'Card2') {
		// 	this.disableInput();
		// 	this.addDummy();
		// } else {
		// 	this.enableInput();
		// }

		// setTimeout(function(){

		// }.bind(this),2000)
		// let 	process = {id:"nsbdbnsi2",type:"processing",content:[]}
		// this.setState({
		// 	messages:[...this.state.messages,process]
		// }) ;
		if (message.type === 'test') {
			message.id = this.keyGenrator() + message.type;
		} else {
			message.id = this.keyGenrator() + message.type;
			message.type = 'user';
		}

		/*let messages = [...this.state.messages,message];
    this.setState({
      messages
    })*/

		if (this.state.mode === 'screening') {
			if (this.state.screeningLoad === false && this.state.interviewLoad === true) {
				let interview_messages = [...this.state.interview, message];
				let messages = [...this.state.messages, message];
				this.setState({
					interview: interview_messages,
					messages: messages,
				});
			} else {
				let messages = [...this.state.screening, message];
				this.setState({
					screening: messages,
					messages: messages,
				});
			}
		} else if (this.state.mode === 'interview') {
			let messages = [...this.state.interview, message];
			this.setState({
				interview: messages,
				messages: messages,
			});
		} else if (this.state.mode === 'general') {
			if (dir === 'up') {
				let messages = [message, ...this.state.general];
				this.setState({
					general: messages,
					messages: messages,
				});
			} else {
				let messages = [...this.state.general, message];
				this.setState({
					general: messages,
					messages: messages,
				});
			}
		}
		if (this.state.mode === 'screening') {
			if (this.state.screeningLoad === false && this.state.interviewLoad === true) {
				axios
					.get(
						'http://149.129.137.48:5009/api/send_interview_message/' +
							this.state.user_data.userId +
							'/' +
							this.state.user_data.jobId +
							'/' +
							message.content
					)
					.then(res => {
						let message = res.data;

						let interview_messages = [...this.state.interview, message];
						let messages = [...this.state.messages, message];
						this.setState({
							loader: 'none',
						});

						console.log(message);
						this.removeDummy();
						this.addDummy();
						if (message.type === 'button' || message.type === 'option') {
							this.disableInput();
						} else {
							this.enableInput();
						}

						this.saveState();
						if ('change' in message) {
							console.log(message);
							this.setState({
								interviewLoad: message.change.interviewLoad,
							});
						}

						this.setState({
							interview: interview_messages,
							messages: messages,
						});
						this.addDummy();
						this.saveState();
					});
			} else {
				axios
					.get(
						'http://149.129.137.48:5009/api/send_message/' +
							this.state.user_data.userId +
							'/' +
							this.state.user_data.jobId +
							'/' +
							message.content
					)
					.then(res => {
						let message = res.data;

						this.addApiMessages(message);
						this.saveState();
						if ('change' in message) {
							this.setState({
								screeningStatus: 'completed',
								interviewStatus: 'running',
							});

							console.log('interview is starting ', message);
							axios
								.get(
									'http://149.129.137.48:5009/api/send_initial_interview_message/' +
										this.state.user_data.jobId +
										'/' +
										this.state.user_data.userId +
										':' +
										'first'
								)
								.then(res => {
									let cards = res.data;
									let time = '';
									cards.map(data => {
										time = data.time;
									});
									var dateFormat = require('dateformat');
									var now = new Date();
									this.setState({
										time: {
											...this.state.time,
											interview_time: time,
										},
									});

									this.addTicker('interview');
									this.setState({
										interview_status_card: [...this.state.interview_status_card, cards[0]],
										buffer: this.state.screening,
									});

									this.setState({
										time: {
											...this.state.time,
											interview_time: this.getRelativeTime(dateFormat(now, 'isoDateTime')),
										},
									});
									this.addDummy();
									this.saveState();
									axios
										.get(
											'http://149.129.137.48:5009/api/start_interview/' +
												this.state.user_data.userId +
												'/' +
												this.state.user_data.jobId
										)
										.then(res => {
											let message = res.data.multiple[0];
											message.id = this.keyGenrator();

											let messages = [...this.state.messages, message];
											console.log(message);

											this.setState({
												interview: [message],
												messages: messages,
											});
											this.setState({
												loader: 'none',
											});

											this.addDummy();
										});
								});

							setTimeout(() => {
								this.setState({
									screeningLoad: message.change.screeningLoad,
								});
								this.saveState();
							}, 1000);
						}
					});
			}
		} else if (this.state.mode === 'interview') {
			axios
				.get(
					'http://149.129.137.48:5009/api/send_interview_message/' +
						this.state.user_data.userId +
						'/' +
						this.state.user_data.jobId +
						'/' +
						message.conten
				)
				.then(res => {
					let message = res.data;
					this.setState({
						loader: 'none',
					});
					this.saveState();
					console.log(message);
					if ('change' in message) {
						console.log(message);
						this.setState({
							interviewLoad: message.change.interviewLoad,
						});
						this.subtaractTicker('interview');
					}

					this.addApiMessages(message);
				});
		} else if (this.state.mode === 'general') {
			let id = '';
			if (this.state.login === true) {
				id = this.state.user_data.userId;
			} else {
				id = this.props.cb.systemId;
			}

			axios.get('http://149.129.137.48:5009/api/search/' + id + '/' + message.content).then(res => {
				let message = res.data;
				this.setState({
					loader: 'none',
				});
				console.log(message);

				this.addApiMessages(message);
				this.saveState();
			});
		}

		// setTimeout(function(){
		// 	let len  = this.state.messages.length;
		// 	this.setState({
		// 	   items: this.state.messages.filter((x,i) => i != len -1 )
		// 	 });
		// },2000).bind(this);
	};
	showScreening = jobId => {
		if (this.state.screeningLoad === true) {
			this.enableInput();
		}
		console.log(this.state.mode);
		this.setState({
			//	messages: this.state.screening,
			chat: true,
			catagory: false,
		});
		this.startScreeening(jobId);
	};
	showInterview = jobId => {
		console.log(this.state.mode);
		this.setState({
			messages: this.state.interview,
			chat: true,
			catagory: false,
		});
		this.startInterview(jobId);
	};
	showGeneral = () => {
		console.log(this.state.mode);
		this.setState({
			messages: this.state.general,
			chat: !this.state.chat,
			catagory: false,
		});
	};

	render() {
		if (this.state.chat) {
			const chat_style = {
				display: this.state.vis.display,
				width: this.state.style.width,
			};
			return (
				<Chat
					loader={this.state.loader}
					resetkey={this.resetkey}
					search_key={this.state.search_key}
					input={this.state.input}
					incCounter={this.incCounter}
					decCounter={this.decCounter}
					addDummy={this.addDummy}
					eof={this.state.eofmessage}
					searchResult={this.searchResult}
					enableInput={this.enableInput}
					disableInput={this.disableInput}
					input={this.state.input}
					saveState={this.saveState}
					back_style={this.state.back_style}
					vis={this.state.vis}
					mobile={this.state.mobile}
					reset={this.reset}
					style={chat_style}
					login={this.state.login}
					title={this.state.mode}
					changeChatStatus={this.changeChatStatus}
					style={this.state.style}
					addMessages={this.addMessages}
					messages={this.state.messages}
					changeGrid={this.changeGrid}
					addParams={this.addParams}
					userdata={this.state.user_data}
					changeCatStatus={this.changeCatStatus}
					grid={this.state.grid}
					changemode={this.changeMode}
					mode={this.state.mode}
					text_search={this.state.search}
					option_button={this.state.button}
					setButton={this.setButton}
					setSearch={this.setSearch}
				/>
			);
		} else if (this.state.catagory) {
			const chat_style = {
				display: this.state.vis.display,
				width: this.state.style.width,
			};
			console.log(chat_style);
			return (
				<div className="chatbot__chat" refs="chat" style={chat_style} onScroll={this.scrollHandler}>
					<Title
						input={this.state.input}
						incCounter={this.state.incCounter}
						decCounter={this.state.decCounter}
						searchResult={this.searchResult}
						enableInput={this.enableInput}
						disableInput={this.disableInput}
						text_search={this.state.text_search}
						setSearch={this.setSearch}
						back_style={this.state.back_style}
						mode={this.state.mode}
						vis={this.state.vis}
						mobile={this.state.mobile}
						style={this.state.style}
						catstyle={this.state.cat_style}
						login={this.state.login}
						title={this.state.mode}
						data={'cat'}
						user_data={this.state.user_data}
						status={this.changeChatStatus}
						key={this.keyGenrator}
						catStatus={this.changeCatStatus}
						changeGrid={this.changeGrid}
						grid={this.state.grid}
					/>
					<Catagory
						tic={this.state.tic}
						resetkey={this.resetkey}
						changeJobId={this.changeJobId}
						saveState={this.saveState}
						time={this.state.time}
						changeToRecommendCard={this.changeToRecommendCard}
						changeToInterviewCard={this.changeToInterviewCard}
						changeToScreenCard={this.changeToScreenCard}
						recommendation_status_card={this.state.recommendation_status_card}
						screening_status_card={this.state.screening_status_card}
						interview_status_card={this.state.interview_status_card}
						vis={this.state.vis}
						grid={this.state.grid}
						catstyle={this.state.cat_style}
						login={this.state.login}
						mode={this.state.mode}
						userdata={this.state.user_data}
						showInterview={this.showInterview}
						showScreening={this.showScreening}
						showGeneral={this.showGeneral}
						key={this.keyGenrator}
						changeMode={this.changeMode}
						catcard={this.state.cat_card}
						mobile={this.state.mobile}
					/>
				</div>
			);
		} else {
			return (
				<Secondary
					login={this.state.login}
					subtaractTicker={this.subtaractTicker}
					tic={this.state.tic}
					login={this.state.login}
					startRecommendation={this.startRecommendation}
					startInterview={this.startInterview}
					startScreeening={this.startScreeening}
					mode={this.state.mode}
					status={this.changeChatStatus}
					catstatus={this.changeCatStatus}
					key={this.keyGenrator}
				/>
			);
		}
	}
}

export default App;
