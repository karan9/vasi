import React, { Component } from "react";
import Send from "./images/send.png";

class Input extends Component {
  state = {
    content: []
    
  
  }
  submitHandler = e => {
    e.preventDefault();
    if (this.state.content.length === 0) {
      console.log(this.state.content);
    } else {
      this.props.addMessages(this.state);

      this.setState({
        content: []
      });
    }
  };
  changeHandler = e => {
    this.setState({
      content: [e.target.value]
    });
  };
  render() {
    return (
      <div className={this.props.input} style={this.props.style}>
        <form onSubmit={this.submitHandler} autocomplete="off">
          <input
            className="chatbot__input"
            style={this.props.style}
            type="text"
            placeholder="Type your query here "
            id="message-input"
            onChange={this.changeHandler}
            value={this.state.content}
          />
          <div onClick={this.submitHandler} className="chatbot__submit">
            <img src={Send} alt="submit-icon not available" />
          </div>
        </form>
      </div>
    );
  }
}

export default Input;
